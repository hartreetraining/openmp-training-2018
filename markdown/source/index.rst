Hartree Centre OpenMP Training Materials and Exercises
======================================================

Introduction
------------
Welcome to the Hartree Centre *Accelerating codes on Intel Processors* Training course. In this course you will learn to evaluate software performance and optimise applications to take full advantage of the latest Intel Xeon and Xeon Phi processors (like the one found on the Scafell Pike machine). For this purpose we will teach multiple aspects of OpenMP 4.5 such as Worksharing, SIMD and Task constructs. We will also showcase the Intel Performance analysis tools: Intel Vector Advisor and Intel Vtune that will help you better understand the performance characteristics of an application. Finally you will learn to use numerical libraries already optimized for the processors.

During the third day we will conduct a *Bring your own code* session.

Lecturers
---------
- Sergi Siso (Hartree Centre)
- Aidan Chalk (Hartree Centre)
- Alin Elena (Scientific Computing Department)
- James Clark (Hartree Centre)
- Heinrich Bockhorst (Intel)
- Klaus-Dieter Oertel (Intel)

Lecture Materials
-----------------

The course introduction presentation can be found at :download:`Welcome slides <./Welcome.pdf>`.

**Materials for the OpenMP part of the course:**

- Lecture 1 is on :download:`OpenMP Worksharing constructs <./OpenMP-worksharing-training.pdf>`. This introduces you to basic OpenMP, such as OpenMP :code:`for`/:code:`do` constructs, as well as the data sharing clauses commonly used in OpenMP programs, such as :code:`shared` and :code:`private`.

- Lecture 2 covers :download:`Vectorisation and the OpenMP SIMD pragmas <./OpenMP-simd-training.pdf>`. The lecture shows why vectorisation is important to utilise modern hardware efficiently, and the options available for you to exploit this feature through OpenMP and language features.

- Lecture 3 covers :download:`Task-Based Parallelism with OpenMP <./OpenMP-task-training.pdf>`. This lecture explains an alternative parallel paradigm, Task-Based Parallelism, and how to use this paradigm with OpenMP.

- Lecture 4 covers :download:`Numerical Libraries <./NumericalLibraries.pdf>`. This lecture is a very gentle introduction to Numerical
Libraries with an emphasis to two commonly used in scientific computing BLAS/LAPACK and FFTW

**Intel MPI and Intel Performance Analysis Tools Material:**

- :download:`Intel MPI 2019 <./intel/Intel_MPI2019.pdf>`

- The :download:`APS Slides <./intel/APS_VTune.pdf>` cover the material for the Application Performance Snapshoot tool. This is a companion :download:`playbook <./intel/APS_Playbook.txt>` which contains instructions for using APS.

- The :download:`Advisor Slides <./intel/Intel_Advisor_for_Vectorization.pdf>` cover the material for Intel Advisor tool. There is also a companion :download:`playbook <./intel/Advisor_Playbook.txt>` which contains instructions to use Advisor XE for vectorisation analysis.

- The :download:`The introduction to VTune Amplifier XE <./intel/Intel_VTune_Intro.pdf>`,  :download:`Memory access profiling using VTune <./intel/Intel_VTune_Memory.pdf>` and :download:`HPC Performance Characterization <./intel/HPC_Characterization.pdf>` slides cover the Intel Vtune Amplifier tool. 

There is an :download:`extras zip file <./intel/intel-extras.zip>` which contains a test code and envrionment scripts required to follow the provided playbooks.


Exercises
---------

Use `git clone  https://gitlab.com/hartreetraining/openmp-training-2018.git` to download the exercises after :doc:`logging into Scafell Pike.</login>`

A pdf version of this webpage is also available :download:`here <./OpenMPTraining.pdf>`.


Table of Contents
-----------------

.. toctree::
   :maxdepth: 1

   Home<index.rst>
   login.rst
   OpenMP-worksharing.rst
   OpenMP-simd.rst
   GameofLife.rst
   MolecularDynamics.rst
   nllab.rst
