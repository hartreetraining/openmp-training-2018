# Vectorisation Exercises


## Vectorization vs No Vectorization

Go to `examples/OpenMP-simd/01-novec`. The directory contains an example code with two identical loops, operating on identical data but one is annotated with `#pragma novector`. Compile the code with `make`, it will use `-qopt-report=5` to gerenate a compiler report.

- What does the optimisation report say about the two loops?
- How much faster do you expect the code to run with the vectorised loop?

Now, run the compiled code with `bsub < knljob.lsf`.

- Why doesn’t the code perform as well as expected?

- What happens if you change the `ARCH` variable in the Makefile? (Remember to `make clean` first)



## Eliminate Dependencies

Go to `examples/OpenMP-simd/02-depend`. This example generates a histogram, based on two input arrays. When you compile the example using `make` and read the optimization reports, the compiler will complain:

```
loop was not vectorized: vector dependence prevents vectorization

remark #15346: vector dependence: assumed FLOW dependence between histogram line 40 and histogram line 40

remark #15346: vector dependence: assumed ANTI dependence between histogram line 40 and histogram line 40
```

Note that it is an assumed dependency by the compiler, but we can eliminate it using Strip Mining technique (turning one loop in to multiple loops).

- Modify the code to implement a Strip Mining solution. What speed-up do you get?
- Change the strip width. Does it change the performance? (Watch out for going out of bounds when accessing arrays!)


## Use restrict keyword

For this exercise go to the `examples/OpenMP-simd/03-restrict` directory. Look
at the `loop.c` file. This example contains two versions of the same function
but one is using the restict type modifier and the other not.

Execute `make` to compile the version without the restrict modifier then read the compiler report at `loop.optrpt`

Then execute `make RESTRICT=1` to compile the version with the restrict modifier and read the new compiler report at `loop.optrpt`

- What do you see in the compiler reports? What has changed? What version do
  you expect to be faster? (you can run both `./restrict-example` to verify)


## OpenMP SIMD

For this exercise go to the `examples/OpenMP-simd/04-omp-simd` directory. We
have again the same example used on the Strip mining exercise, but in this
case, instead of modifying the original code, we aim to provide solve the
dependency by just adding an OpenMP SIMD pragma.

- Replace the `ADD OPENMP SIMD PRAGMA` with an OpenMP pragma. Which command did
  you decide to use? Can we also specify any property regarding the memory?

- Does the compiler eliminate the assumend dependency and improve the
performance?


## Optional exercise

Go back to the Jacobi example of the OpenMP-worksharing exercises. Compile the
optimized version of the Jacobi algorithm with `-qopt-report=5`.

- What is the difference in performance between compiling with auto-vectorization enabled (default) and auto-vectorization disabled (flags: -no-simd -no-vec)?

- Are there any parts of the code that can be improved by
  vectorization? alignment of data structures?

