!    Copyright (c) 2014-2016 Alin Marin Elena <alinm.elena@gmail.com>
!    The MIT License http://opensource.org/licenses/MIT
program fftw1D
  use omp_lib
  use :: iso_c_binding
  implicit none
  include 'fftw3.f03'

  integer, parameter :: pr=kind(1.0d0)
  real (kind=pr), parameter :: pi = 4.0_pr*atan(1.0_pr)

  integer :: n,nSamples
  logical :: isScaled

  complex(c_double_complex), dimension(:),allocatable :: a, b, aold
  !DIR$ATTRIBUTES ALIGN:64 :: a,b  

  real (kind=pr), allocatable :: times(:)
  character(len=32) :: arg,name,sT,sF
  integer :: ierror,i
  real(kind=pr) :: mint,maxt,avgt,s,fl 
  type(c_ptr) :: forward, backward

  if (command_argument_count() /=3)  then
    call get_command_argument(0, name)
    write(*,*)"wrong numbers of parameters. Correct usage: "
    write(*,*) trim(name)//" <N> <nSamples> <Scaled?>"
    write(*,*)trim(name)//" 100 10 T"
    stop
  endif 

  call get_command_argument(1, arg)
  read(arg,*) n
  call get_command_argument(2, arg)
  read(arg,*) nSamples
  call get_command_argument(3, arg)
  read(arg,*)isScaled

  write(*,'(a,i0)')"Running with N=",n
  write(*,'(a,i0)')"Size of sample is ",nSamples
  sT="On";sF="Off"
  write(*,'(a)')"Scaling is "//merge(sT,sF,isScaled)
  write(*,'(a,g16.8,a)')"Needs to allocate ",n*3.0_8*16.0/1024.0/1024.0/1024.0," GiB"

  allocate(a(n),b(n),aold(n),stat=ierror)
  if (ierror/=0) then
    write(*,*) "Error allocating memory for FFT"
    stop
  endif

  allocate(times(nSamples),stat=ierror)
  if (ierror/=0) then
    write(*,*) "Error allocating memory for timing array"
    stop
  endif

  if (fftw_init_threads() /=0) then
    sT=" thread";sF=" threads"
    write(*,'(a,i0,a)')"Setting up ", omp_get_max_threads(),merge(sT,sF, omp_get_max_threads()==1)
  else
    write(*,*)"Failure to setup threads!!!"
    stop
  endif
  call fftw_plan_with_nthreads(omp_get_max_threads())  

!$omp master
  forward=fftw_plan_dft_1d(n, a, b, FFTW_FORWARD, FFTW_MEASURE)
  backward=fftw_plan_dft_1d(n, b, aold, FFTW_BACKWARD, FFTW_MEASURE)        
!$omp end master    

  do i = 1,n
    a(i) = cmplx(sin((2*pi)*((i-1.0)/(n-1))), 0.0_pr)
    b(i) = cmplx(0.0_pr,0.0_pr)
    aold(i) = a(i) 
  end do
  write(*,'(a)') "Crunching..."
  if (.not.isScaled) then
    do i = 1,nSamples
      times(i) = omp_get_wtime()
      call fftw_execute_dft(forward, a, b)
      call fftw_execute_dft(backward, b, a)
      times(i) = omp_get_wtime()-times(i)
    end do
  else
    do i = 1,nSamples
      times(i) = omp_get_wtime()
      call fftw_execute_dft(forward, a, b)
      b = b/real(n,pr)
      call fftw_execute_dft(backward, b, a)
      times(i) = omp_get_wtime()-times(i)
    end do
  endif

  !$OMP MASTER
  call fftw_destroy_plan(forward)
  call fftw_destroy_plan(backward)
  !$OMP END MASTER

  if (isScaled) then 
    s=0.0_pr
    do i = 1,n
      s=s+(real(a(i),pr)-real(aold(i),pr))*(real(a(i),pr)+real(aold(i),pr))
    end do
  endif

  write(*,'(a)')"#Loop  |     Size |       Time (s) "
  do i=1,nSamples
    write(*,'(i8,1x,i10,1x,g16.8)')i,n,times(i)
  enddo

  mint=minval(times)
  maxt=maxval(times)
  avgt=(sum(times)-mint-maxt)/(nSamples-2)

  if (isScaled) write(*,'(a,g16.8)')"Accuracy test: ",s
  write(*,'(a)')"#Size     | Min. Time (s) | Max. Time (s) | Avg. Time (s) | Total time (s)" 
  write(*,'(i10,3(g16.4),g16.4)')n, mint,maxt,avgt,sum(times)
  fl=2.0_pr*5.0_pr*n*log(real(n,pr))/log(2.0)/1000000000.0
  write(*,'(a10,3a16)')"#Size|", "GFlops min |", "GFlops max |", "GFlops avg" 
  write(*,'(i10,3(g16.4))')n, fl/maxt,fl/mint,fl/avgt

  call fftw_cleanup_threads()
  call fftw_cleanup()
  deallocate(a,b,aold)
  deallocate(times)

end program fftw1D

