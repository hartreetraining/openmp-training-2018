/*    Copyright (c) 2014-2016 Alin Marin Elena <alinm.elena@gmail.com>
      The MIT License http://opensource.org/licenses/MIT*/
#include <stdlib.h>
#include <stdio.h>
#include <complex.h>
#include <fftw/fftw3.h>
#include <stdbool.h>
#include <omp.h>
#include <math.h>
#define PI (3.141592653589793)
int main(int argc, char **argv) {
  fftw_complex *a,*b,*aold;
  fftw_plan backward,forward;
  if (argc!=4){
    printf("Wrong number of arguments!!! \n");
    printf("usage: %s <N> <nSamples> <Scaled?> \n",argv[0]);
    printf("usage: %s 1000 10 1\n",argv[0]);
    printf("usage: 0-false; 1-true \n");
    return -1;
  }
  int n=atoi(argv[1]);
  int nrep=atoi(argv[2]);
  bool isScaled=atoi(argv[3]);

  printf("Running with N=%d\n",n);
  printf("Sampling size is %d \n",nrep);
  printf("Scaling is %s\n",isScaled?"On":"Off");
  printf("Memory required is %e GiB\n",(float)n*3.0*sizeof(fftw_complex)/1024.0/1024.0/1024.0);

  int ans=posix_memalign((void *)&a,64,n*sizeof(fftw_complex));
  if (ans!=0) printf("I cannot allocate %s!\n","a");
  ans+=posix_memalign((void *)&b,64,n*sizeof(fftw_complex));
  if (ans!=0) printf("I cannot allocate %s!\n","b");
  ans+=posix_memalign((void *)&aold,64,n*sizeof(fftw_complex));
  if (ans!=0) {
    printf("I cannot allocate %s!\n","aold");
    return -1;
  }
  double *times = (double *) malloc(nrep*sizeof(double));

  if (fftw_init_threads() !=0) {
    printf("Setting up %d %s\n", omp_get_max_threads(),omp_get_max_threads()==1?"thread":"threads");
  }
  else{
    printf("I cannot setup the threads\n");
    return -2;
  }
  fftw_plan_with_nthreads(omp_get_max_threads());
#pragma omp master
  {
    forward=fftw_plan_dft_1d(n, a, b, FFTW_FORWARD, FFTW_MEASURE);
    backward=fftw_plan_dft_1d(n, b, aold, FFTW_BACKWARD, FFTW_MEASURE);        
  }
  for (int i=0;i<n;++i){
    a[i] = sin((2*PI)*((i-1.0)/(n-1)))+0*I;
    b[i] = 0.0+0.0*I;
    aold[i] = a[i];
  }
  printf("Crunching...\n");
  if (isScaled){
    for(int i = 0;i<nrep;++i){
      times[i] = omp_get_wtime();
      fftw_execute_dft(forward, a, b);
      for(int j=0;j<n;++j) b[j] = b[j]/(double)(n);
      fftw_execute_dft(backward,b, a);
      times[i] = omp_get_wtime()-times[i];
    }
  }else{
    for(int i = 0;i<nrep;++i){
      times[i] = omp_get_wtime();
      fftw_execute_dft(forward, a, b);
      fftw_execute_dft(backward, b, a);
      times[i] = omp_get_wtime()-times[i];
    }
  }
#pragma omp master
  {
    fftw_destroy_plan(forward);
    fftw_destroy_plan(backward);
  }
  double s;
  if (isScaled){
    s-0.0;
    for(int i=0;i<n;++i){
      s+=(creal(a[i])-creal(aold[i]))*(creal(a[i])-creal(aold[i]));
    }
  }
  printf("%5s|%10s|%12s\n","#Loop","Size","Times[s]");
  for(int i=0;i<nrep;++i)printf("%5d %10d %12.6e\n",i+1,n,times[i]);
  if (isScaled) printf("Accuracy test %e\n",s);

  double tmin=42000.0;double tmax=0.0;
  double ttot=0.0;
  for (int i=0;i<nrep;++i){
    if (tmin>times[i]) tmin=times[i];
    if (tmax<times[i]) tmax=times[i];
    ttot+=times[i];
  }
  double tavg=(ttot-tmin-tmax)/(nrep-2);
  printf("%11s|%12s|%12s|%12s|%12s\n","#Size","Tmin[s]","Tmax[s]","Tavg[s]","Ttot[s]");
  printf("%11d %12.4e %12.4e %12.4e %12.4e\n",n,tmin,tmax,tavg,ttot);
  printf("%11s|%12s|%12s|%12s\n","#Size","GFlops min","GFlops max","GFlops avg");
  double fl=2*5.0*n*log(n)/log(2.0)/1000000000.0;
  printf("%11d %12.6e %12.6e %12.6e\n",n,fl/tmax,fl/tmin,fl/tavg);
  /*
     FILE *fp=fopen("c.777","w");
     for(int i=0;i<n;++i)fprintf(fp,"%d %e\n",i,creal(a[i]));
     fclose(fp);
     */
  fftw_cleanup_threads();
  fftw_cleanup();
  free(a);
  free(aold);
  free(b);
  free(times);
  return 0;
}
