/*    Copyright (c) 2014-2016 Alin Marin Elena <alinm.elena@gmail.com>
      The MIT License http://opensource.org/licenses/MIT*/
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include "flops.h"
#include <mkl.h> //this is included for timing routine
#include <omp.h>

void initMatrix(double **A,int n, int m, double d);
int createMatrix(double ***A, int n, int m, char *label);
void destroyMatrix(double **A,int n);
void MxM(double **C,double **A, double **B,int n, int k, int m);

int main(int argc, char **argv) {

  int n,k,m;
  double **A,**B,**C;
  int ans;
  int i,j;

  if (argc!=5){
    printf("Wrong number of arguments!!! \n");
    printf("usage: %s  nMatrix kMatrix mMatrix NSamples\n",argv[0]);
    return -1;
  }

  n=atoi(argv[1]);
  k=atoi(argv[2]);
  m=atoi(argv[3]);
  int nSamples=atoi(argv[4]);
#pragma omp parallel
#pragma omp master
  printf("Using %d threads ouf of a maximum of %d\n",omp_get_num_threads(),omp_get_max_threads());

  printf("Running... ");
  for(i=0;i<argc;++i){
    printf("%s ",argv[i]);
  }
  printf("\n");

  double *times = (double *) malloc(nSamples*sizeof(double));
  ans=createMatrix(&A,n,k,"A"); 
  ans=createMatrix(&B,k,m,"B"); 
  ans=createMatrix(&C,n,m,"C"); 
  double tmin=DBL_MAX;double tmax=0.0;
  for (i=1; i<=nSamples; ++i) {
    initMatrix(A,n,k,0.5*i);
    initMatrix(B,k,m,0.75*i);
    initMatrix(C,n,m,0.0);
    times[i-1] = dsecnd();
    MxM(C,A,B,n,k,m);
    times[i-1] = dsecnd()-times[i-1];
    if (tmin>times[i-1]) tmin=times[i-1];
    if (tmax<times[i-1]) tmax=times[i-1];

  }
  double timeAvg = 0.0;
  for(i=0;i<nSamples;++i){
    timeAvg+=times[i];
  }
  timeAvg/=nSamples;
  double ops = FLOPS_DGEMM(n,k,m);

  double sig=0.0;
  for(i=0;i<nSamples;++i){      
    sig+=(ops*1e-9/times[i]-ops*1e-9/timeAvg)*(ops*1e-9/times[i]-ops*1e-9/timeAvg);
  }
  sig=sqrt(sig/nSamples);

  printf("#Loop | Sizes n,k,m        |   Time (s)|  GFlop/s\n");
  for(i=0;i<nSamples;++i){
    printf("%6d %6d %6d %6d %11.4f %11.4f\n",i+1,n,k,m,times[i],ops*1e-9/times[i]);
  }
  printf("\nThe (1:4,1:4) block of the result:\n");
  for(i=0;i<4;++i){
    for(j=0;j<4;++j){
      printf("%15.8e ",C[i][j]);
    }
    printf("\n");
  }
  printf("\nSummary:\n");
  printf("#Sizes n,k,m        |  Avg. Time (s) |   Avg. Gflop/s |   Min. GFlop/s |   Max. GFlop/s | σ GFlop/s\n");
  printf("%6d %6d %6d %16.8f %16.8f %16.8f %16.8f %16.8f\n",n,k,m,timeAvg,ops*1e-9/timeAvg,ops*1e-9/tmax,ops*1e-9/tmin,sig);

  free(times);
  destroyMatrix(A,n);
  destroyMatrix(B,k);
  destroyMatrix(C,n);
  return 0;
}

void initMatrix(double **A,int n,int m,double d){
  int i,j;
#pragma omp for schedule (static) private(i,j)
  for(i=0;i<n;++i){
    for(j=0;j<m;++j){
      A[i][j]=d*(double)(i-j)/(n);
    }
  }
}

int createMatrix(double ***X, int n, int m , char *label){
  int i; 
  double **A;
  printf("Matrix %s sizes %dx%d. Memory requested for %d elements, that is %12.8E GiB\n",label,n,m,n*m,n*m*sizeof(double)/1024.0/1024.0/1024);
  A=(double **)malloc(n*sizeof(double *));
  if (A == NULL){
    printf("I cannot allocate A!\n");
    return -1;
  }
  for(i=0;i<n;++i){
    A[i]=(double *)malloc(m*sizeof(double));
    if (A[i] == NULL){
      printf("I cannot allocate %s[%d]!\n",label,i);
      return -1;
    }
  }
  *X=A;
  return 0;
}

void destroyMatrix(double **A,int n){
  int i;

  for(i=0;i<n;++i){
    free((void *)A[i]);
  }
  free((void*)A);
}

void MxM(double **C,double **A, double **B,int n, int k, int m){
  int i,j,l;
  for(i=0;i<n;++i){
    for(j=0;j<m;++j){
      for(l=0;l<k;++l){
        C[i][j]+=A[i][l]*B[l][j];
      }
    }
  }

}
