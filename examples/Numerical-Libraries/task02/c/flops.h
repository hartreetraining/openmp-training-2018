/*    Copyright (c) 2014-2016 Alin Marin Elena <alinm.elena@gmail.com>
      The MIT License http://opensource.org/licenses/MIT*/
#ifndef _FLOPS_H_
#define _FLOPS_H_

#define FMULS_GEMM(n, k, m) ((n) * (k) * (m))
#define FADDS_GEMM(n, k, m) ((n) * (k) * (m))

#define FLOPS_DGEMM(n, k, m) (     FMULS_GEMM((double)(n), (double)(k), (double)(m)) +       FADDS_GEMM((double)(n), (double)(k), (double)(m)) )

#endif /* _FLOPS_H_ */
