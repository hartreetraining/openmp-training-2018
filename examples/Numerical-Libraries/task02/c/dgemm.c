/*    Copyright (c) 2014-2016 Alin Marin Elena <alinm.elena@gmail.com>
      The MIT License http://opensource.org/licenses/MIT*/
#include <mkl.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include "flops.h"
#include <omp.h>

void initMatrix(double *A,int n, int m, double d);
int createMatrix(double **X, int n, int m, char *label);
int main(int argc, char **argv) {

  MKL_INT n,k,m;
  double *A,*B,*C;
  int ans;
  int i,j;

  if (argc!=5){
    printf("Wrong number of arguments!!! \n");
    printf("usage: %s  nMatrix kMatrix mMatrix NSamples\n",argv[0]);
    return -1;
  }

  n=atoi(argv[1]);
  k=atoi(argv[2]);
  m=atoi(argv[3]);
  int nSamples=atoi(argv[4]);
#pragma omp parallel
#pragma omp master
  printf("Using %d threads ouf of a maximum of %d\n",omp_get_num_threads(),omp_get_max_threads());

  printf("Running... ");
  for(i=0;i<argc;++i){
    printf("%s ",argv[i]);
  }
  printf("\n");

  double *times = (double *) malloc(nSamples*sizeof(double));
  ans=createMatrix(&A,n,k,"A");
  ans=createMatrix(&B,k,m,"B");
  ans=createMatrix(&C,n,m,"C");
  initMatrix(A,n,k,0.5);
  initMatrix(B,k,m,0.75);
  initMatrix(C,n,m,0.0);
  double alpha = 1.0, beta = 0.0;
  // first call which does the thread/buffer initialization
  DGEMM("N", "N", &n, &m, &k, &alpha, A, &n, B, &k, &beta, C, &n);
  // start timing after the first GEMM call
  double tmin=DBL_MAX;double tmax=0.0;
  for (i=1; i<=nSamples; ++i) {
    initMatrix(A,n,k,0.5*i);
    initMatrix(B,k,m,0.75*i);
    initMatrix(C,n,m,0.0);
    times[i-1] = dsecnd();
    DGEMM("N", "N", &n, &m, &k, &alpha, A, &n, B, &k, &beta, C, &n);
    times[i-1] = dsecnd()-times[i-1];

    if (tmin>times[i-1]) tmin=times[i-1];
    if (tmax<times[i-1]) tmax=times[i-1];

  }
  double timeAvg = 0.0;
  for(i=0;i<nSamples;++i){
    timeAvg+=times[i];
  }
  timeAvg/=nSamples;
  double ops = FLOPS_DGEMM(n,k,m);

  double sig=0.0;
  for(i=0;i<nSamples;++i){      
    sig+=(ops*1e-9/times[i]-ops*1e-9/timeAvg)*(ops*1e-9/times[i]-ops*1e-9/timeAvg);
  }
  sig=sqrt(sig/nSamples);

  printf("#Loop | Sizes n,k,m        |   Time (s)|  GFlop/s\n");
  for(i=0;i<nSamples;++i){
    printf("%6d %6d %6d %6d %11.4f %11.4f\n",i+1,n,k,m,times[i],ops*1e-9/times[i]);
  }
  printf("\nThe (1:4,1:4) block of the result:\n");
  for(i=0;i<4;++i){
    for(j=0;j<4;++j){
      printf("%15.8e ",C[i*n+j]);
    }
    printf("\n");
  }
  printf("\nSummary:\n");
  printf("#Sizes n,k,m        |  Avg. Time (s) |   Avg. Gflop/s |   Min. GFlop/s |   Max. GFlop/s | σ GFlop/s\n");
  printf("%6d %6d %6d %16.8f %16.8f %16.8f %16.8f %16.8f\n",n,k,m,timeAvg,ops*1e-9/timeAvg,ops*1e-9/tmax,ops*1e-9/tmin,sig);

  free(times);
  free(C);
  free(B);
  free(A);

  return 0;
}

int createMatrix(double **X, int n, int m, char *label){
  int i;
  double *A;
  int ans;

  printf("Matrix %s sizes %dx%d. Memory requested for %d elements, that is %12.8E GiB\n",label,n,m,n*m,n*m*sizeof(double)/1024.0/1024.0/1024);

  ans=posix_memalign((void **)&A,64,n*m*sizeof(double));
  if (ans!=0) {
    printf("I cannot allocate %s!\n",label);
    printf("requested %d elements, that is %16.8f bytes\n",n*m,(double)n*m*sizeof(double));
  }
  *X=A;
  return ans;
}
void initMatrix(double *A,int n,int m,double d){
  int i,j;
#pragma omp for schedule (static) private(i,j)
  for(i=0;i<n;++i){
    for(j=0;j<m;++j){
      A[i*n+j]=d*(double)(i-j)/(n);
    }
  }
}
