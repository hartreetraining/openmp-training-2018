#include "md.h"
#include <stdio.h>
#include <omp.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include "twister.h"

void update(struct data *md_data) {
  int i, j;
  double rmass;

  rmass = 1.0 / mass;
#pragma omp parallel for default(none) private(i) shared(rmass, md_data)
  for (i = 0; i < nparts; i++) {

    md_data->x_pos[i] +=
        md_data->x_vel[i] * dt + 0.5 * dt * dt * md_data->x_accel[i];
    md_data->y_pos[i] +=
        md_data->y_vel[i] * dt + 0.5 * dt * dt * md_data->y_accel[i];
    md_data->z_pos[i] +=
        md_data->z_vel[i] * dt + 0.5 * dt * dt * md_data->z_accel[i];

    md_data->x_vel[i] +=
        0.5 * dt * (md_data->x_force[i] * rmass + md_data->x_accel[i]);
    md_data->y_vel[i] +=
        0.5 * dt * (md_data->y_force[i] * rmass + md_data->y_accel[i]);
    md_data->z_vel[i] +=
        0.5 * dt * (md_data->z_force[i] * rmass + md_data->z_accel[i]);

    md_data->x_accel[i] = md_data->x_force[i] * rmass;
    md_data->y_accel[i] = md_data->y_force[i] * rmass;
    md_data->z_accel[i] = md_data->z_force[i] * rmass;
  }
}

void compute_step(struct data *md_data) {

  int i, j, k;
  double rxx, ryy, rzz, rrr, rsq, ir, irsq, cutoffs, force, energy;
  double sig_r, sig_r2, sig_r6, sig_r12;
  double t_x, t_y, t_z;
  double t_e[512];
  static double t_t_e;

#pragma omp parallel default(none) private(                                    \
    t_x, t_y, t_z, i, j, rxx, ryy, rzz, cutoffs, rsq, rrr, sig_r, sig_r2,      \
    sig_r6, sig_r12, force, energy) shared(md_data, t_e) private(t_t_e)
  {
    t_e[omp_get_thread_num()] = 0.0;
#pragma omp single
    {
// One-sided force interaction
#pragma omp taskloop grainsize(16) default(none) private(                      \
    t_t_e, i, j, t_x, t_y, t_z, rxx, ryy, rzz, cutoffs, rsq, rrr, sig_r,       \
    sig_r2, sig_r6, sig_r12, force, energy) shared(md_data, t_e)
      for (i = 0; i < nparts; i++) {
        t_x = 0.0;
        t_y = 0.0;
        t_z = 0.0;
        t_t_e = 0.0;
        for (j = 0; j < nparts; j++) {
          if (i == j)
            continue; //            if(i!=j){
          rxx = md_data->x_pos[i] - md_data->x_pos[j];
          ryy = md_data->y_pos[i] - md_data->y_pos[j];
          rzz = md_data->z_pos[i] - md_data->z_pos[j];
          cutoffs = fmaxf(md_data->cutoff[i], md_data->cutoff[j]);
          rsq = rxx * rxx + ryy * ryy + rzz * rzz;
          if (rsq < cutoffs * cutoffs) {
            // Compute interaction
            rrr = sqrt(rsq);
            sig_r = sigma / rrr;
            sig_r2 = sig_r * sig_r;
            sig_r6 = sig_r2 * sig_r2 * sig_r2;
            sig_r12 = sig_r6 * sig_r6;
            force = 4.0 * eps * (sig_r12 - sig_r6);
            energy = 4.0 * eps * sig_r6 * (sig_r6 - 1.0);

            // Store energy due to particle i
            t_t_e += 0.5 * energy;
            t_x += force * rxx;
            t_y += force * ryy;
            t_z += force * rzz;
          }
        }
        md_data->x_force[i] = t_x;
        md_data->y_force[i] = t_y;
        md_data->z_force[i] = t_z;
        t_e[omp_get_thread_num()] += t_t_e;
      }
    }
#pragma omp atomic
    md_data->E0 += t_e[omp_get_thread_num()];
  }
}

void destruct(struct data *md_data) {

  free(md_data->x_pos);
  free(md_data->y_pos);
  free(md_data->z_pos);
  free(md_data->x_vel);
  free(md_data->y_vel);
  free(md_data->z_vel);
  free(md_data->x_force);
  free(md_data->y_force);
  free(md_data->z_force);
  free(md_data->x_accel);
  free(md_data->y_accel);
  free(md_data->z_accel);
  free(md_data->cutoff);
}

void initialise(struct data *md_data) {
  int i, j, k;

  // Allocate all of of the arrays.
  md_data->x_pos = malloc(sizeof(double) * nparts);
  md_data->y_pos = malloc(sizeof(double) * nparts);
  md_data->z_pos = malloc(sizeof(double) * nparts);
  md_data->x_vel = calloc(nparts, sizeof(double));
  md_data->y_vel = calloc(nparts, sizeof(double));
  md_data->z_vel = calloc(nparts, sizeof(double));
  md_data->x_force = calloc(nparts, sizeof(double));
  md_data->y_force = calloc(nparts, sizeof(double));
  md_data->z_force = calloc(nparts, sizeof(double));
  md_data->x_accel = calloc(nparts, sizeof(double));
  md_data->y_accel = calloc(nparts, sizeof(double));
  md_data->z_accel = calloc(nparts, sizeof(double));
  md_data->cutoff = malloc(sizeof(double) * nparts);

  twister_Initialize(0);

  for (i = 0; i < nparts; i++) {
    md_data->x_pos[i] = ((double)ExtractU32() / (double)MAX_RAND) * box[0];
    md_data->y_pos[i] = ((double)ExtractU32() / (double)MAX_RAND) * box[1];
    md_data->z_pos[i] = ((double)ExtractU32() / (double)MAX_RAND) * box[2];
    md_data->cutoff[i] = ((double)(i + 1)) * (box[0] / (double)nparts);
  }
  md_data->E0 = 0.0;
}

void main() {
  double t1, t;
  struct data md_data;

  initialise(&md_data);

  compute_step(&md_data);
  t = 0.0;
  for (int i = 0; i < nsteps; i++) {
    md_data.E0 = 0.;
    t1 = omp_get_wtime();
    compute_step(&md_data);
    update(&md_data);
    t += omp_get_wtime() - t1;
    printf("Energy: %f\n", md_data.E0);
  }
  printf("Runtime for loop iterations:%f s\n", t);
  destruct(&md_data);
}
