ifeq ($(CC), )
  CC=gcc
endif
CFLAGS=-O3 -g -std=c99
ifeq ($(CC),icc)
	 CFLAGS+=-qopenmp -xHost
else
	 CFLAGS+=-fopenmp -march=native
endif

DEF_WIDTH=4096
DEF_HEIGHT=2399
DEF_TIMESTEPS=256

TIMING_WIDTH=4096
TIMING_HEIGHT=64
TIMING_TIMESTEPS=16

OUTPUT_WIDTH=64
OUTPUT_HEIGHT=64
OUTPUT_TIMESTEPS=16

DEF_FLAGS=-DWIDTH=$(DEF_WIDTH) -DHEIGHT=$(DEF_HEIGHT) -DTIMESTEPS=$(DEF_TIMESTEPS)
TIMING_FLAGS=-DWIDTH=$(TIMING_WIDTH) -DHEIGHT=$(TIMING_HEIGHT) -DTIMESTEPS=$(TIMING_TIMESTEPS) -DTIMNIG
OUTPUT_FLAGS=-DWIDTH=$(OUTPUT_WIDTH) -DHEIGHT=$(OUTPUT_HEIGHT) -DTIMESTEPS=$(OUTPUT_TIMESTEPS) -DOUTPUTS

OMPEXEC=bin/omp
TASKEXEC=bin/task
TASK2EXEC=bin/depend

OMPTIMINGEXEC=bin/omptiming
TASKTIMINGEXEC=bin/tasktiming
TASK2TIMINGEXEC=bin/dependtiming

OMPSOURCE=src/GoLomp.c
TASKSOURCE=src/GoLtask.c
TASK2SOURCE=src/GoLtask2.c

OMPOUTPUTS=bin/ompoutputs

.PHONY: default

default: $(OMPEXEC) $(TASKEXEC) $(TASK2EXEC)


bin:
	mkdir -p bin

outputs:
	mkdir -p outputs

.PHONY: all

all: default timing

$(OMPEXEC): $(OMPSOURCE) bin outputs
	$(CC) $(CFLAGS) $(DEF_FLAGS) $(OMPSOURCE) -o $@

$(TASKEXEC): $(TASKSOURCE) bin outputs
	$(CC) $(CFLAGS) $(DEF_FLAGS) $(TASKSOURCE) -o $@

$(TASK2EXEC): $(TASK2SOURCE) bin outputs
	$(CC) $(CFLAGS) $(DEF_FLAGS) $(TASK2SOURCE) -o $@

.PHONY: timing

timing: $(OMPTIMINGEXEC) $(TASKTIMINGEXEC) $(TASK2TIMINGEXEC)

$(OMPTIMINGEXEC): $(OMPSOURCE) bin
	$(CC) $(CFLAGS) $(TIMING_FLAGS) $(OMPSOURCE) -o $@

$(TASKTIMINGEXEC): $(TASKSOURCE) bin
	$(CC) $(CFLAGS) $(TIMING_FLAGS) $(TASKSOURCE) -o $@

$(TASK2TIMINGEXEC): $(TASK2SOURCE) bin
	$(CC) $(CFLAGS) $(TIMING_FLAGS) $(TASK2SOURCE) -o $@

.PHONY: output

output: $(OMPOUTPUTS)


$(OMPOUTPUTS): $(OMPSOURCE) bin outputs
	$(CC) $(CFLAGS) $(OUTPUT_FLAGS) $(OMPSOURCE) -o $@


.PHONY: clean

clean: 
	rm -rf $(OMPEXEC) $(TASKEXEC) $(TASK2EXEC) $(OMPTIMINGEXEC) $(TASKTIMINGEXEC) $(TASK2TIMINGEXEC) $(OMPOUTPUTS) 2> /dev/null
