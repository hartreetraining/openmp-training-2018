#include "loop.h"

#ifdef RESTRICT
void do_work(int n, REAL * restrict a, REAL * restrict x, REAL * restrict y) {
    for (int i = 1; i < n; i++) {
        a[i] += x[i-1];
        y[i] += a[i] + x[i];
    }
}
#else
void do_work(int n, REAL * a, REAL * x, REAL * y) {
    for (int i = 1; i < n; i++) {
        a[i] += x[i-1];
        y[i] += a[i] + x[i];
    }
}
#endif

