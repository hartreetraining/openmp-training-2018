# include <math.h>
# include <stdio.h>
# include <stdlib.h>

int main ( )

/*
  Purpose:
    MAIN is the main program for JACOBI_OPENMP.
  Discussion:
    JACOBI_OPENMP carries out a Jacobi iteration with OpenMP.
  Licensing:
    This code is distributed under the GNU LGPL license. 
  Modified:
    19 February 2020
  Author:
    John Burkardt
    Modified STFC
*/

{
  double *input;
  double diff;
  int i;
  int it;
  int const n_iterations = 5000;
  int const n_variables = 500000;
  double residual;
  double t;
  double *solution;
  double *iterative_solution;

  input = ( double * ) malloc ( n_variables * sizeof ( double ) );
  solution = ( double * ) malloc ( n_variables * sizeof ( double ) );
  iterative_solution = ( double * ) malloc ( n_variables * sizeof ( double ) );

  printf ( "\n" );
  printf ( "JACOBI_OPENMP:\n" );
  printf ( "  C/OpenMP version\n" );
  printf ( "  Jacobi iteration to solve A*x=b.\n" );
  printf ( "\n" );
  printf ( "  Number of variables  N = %d\n", n_variables );
  printf ( "  Number of iterations M = %d\n", n_iterations );

  printf ( "\n" );
  printf ( "  IT     l2(dX)    l2(resid)\n" );
  printf ( "\n" );


  // Set up the right hand side.
  // ADD OPENMP PRAGMA ################
  for ( i = 0; i < n_variables; i++ ){
    input[i] = 0.0;
  }

  input[n_variables-1] = ( double ) ( n_variables + 1 );


  // Initialize the solution estimate to 0.
  // Exact solution is (1,2,3,...,N).
  // ADD OPENMP PRAGMA ################
  for ( i = 0; i < n_variables; i++ ){
    solution[i] = 0.0;
  }

  // Iterate M times.
  for ( it = 0; it < n_iterations; it++ ){

	  // Jacobi update.
      // ADD OPENMP PRAGMA ################
	  for ( i = 0; i < n_variables; i++ ){
		iterative_solution[i] = input[i];
		if ( 0 < i ) iterative_solution[i] = iterative_solution[i] + solution[i-1];
		if ( i < n_variables - 1 ) iterative_solution[i] = iterative_solution[i] + solution[i+1];
		iterative_solution[i] = iterative_solution[i] / 2.0;
	  }

	  // Difference.
	  diff = 0.0;
      // ADD OPENMP PRAGMA ################
	  for ( i = 0; i < n_variables; i++ ) {
		diff = diff + pow ( solution[i] - iterative_solution[i], 2 );
	  }

	  // Overwrite old solution.
      // ADD OPENMP PRAGMA ################
	  for ( i = 0; i < n_variables; i++ ){
		solution[i] = iterative_solution[i];
	  }

	  // Residual.
	  residual = 0.0;
      // ADD OPENMP PRAGMA ################
	  for ( i = 0; i < n_variables; i++ ){
		t = input[i] - 2.0 * solution[i];
		if ( 0 < i ) t = t + solution[i-1];
		if ( i < n_variables - 1 ) t = t + solution[i+1];
		residual = residual + t * t;
	  }

	  if ( it < 10 || n_iterations - 10 < it ){
		  printf ( "  %8d  %14.6g  %14.6g\n", it, sqrt ( diff ), sqrt ( residual ) );
	  }
	  if ( it == 9 ){
		printf ( "  Omitting intermediate results.\n" );
	  }

  }

  // Write part of final estimate.
  printf ( "\n" );
  printf ( "  Part of final solution estimate:\n" );
  printf ( "\n" );
  for ( i = 0; i < 10; i++ ){
    printf ( "  %8d  %14.6g\n", i, solution[i] );
  }
  printf ( "...\n" );
  for ( i = n_variables - 11; i < n_variables; i++ ){
    printf ( "  %8d  %14.6g\n", i, solution[i] );
  }

  // Free memory.
  free ( input );
  free ( solution );
  free ( iterative_solution );

  // Terminate.
  printf ( "\n" );
  printf ( "JACOBI_OPENMP:\n" );
  printf ( "  Normal end of execution.\n" );

  return 0;
}

